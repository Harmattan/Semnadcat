import de.svenjacobs.loremipsum.LoremIpsum;

import javax.swing.*;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


public class Semnadcat1 {

    public static void main(String[] args) throws IOException {

        String filePath = "C:\\Users\\User\\Desktop\\Project\\Text.txt";
        final File FILE_TEXT = new File(filePath);

        // получение рандомных слов
        LoremIpsum loremIpsum = new LoremIpsum();
        int sizeOfIpsum = new Random().nextInt(1000) + 10;
        System.out.println("sizeOfIpsum = " + sizeOfIpsum);
        String randomText = loremIpsum.getWords(sizeOfIpsum);
        System.out.println("randomText = \"" + randomText + "\"");

        // запись слов в файл
        try(FileWriter writer = new FileWriter(filePath, false))
        {
            // запись всей строки
            writer.write(randomText);
            writer.flush();
        }
        catch(IOException ex){
            throw new IOException(ex);
        }

        // чтение файла
        String resultStr = ReadFile.read(FILE_TEXT);
        String[] resultMass;
        resultMass = resultStr.toLowerCase().replaceAll("[-.?!)(,:]", "").split("\\s");

        Map<String, Integer> counterMap = new HashMap<>();
        for (String word : resultMass) {
            if (!word.isEmpty()) {
                Integer count = counterMap.get(word);
                if (count == null) {
                    count = 0;
                }
                counterMap.put(word, ++count);
            }
        }

        for (String word : counterMap.keySet()) {
            System.out.println(word + ": " + counterMap.get(word));
        }
    }
}
class ReadFile {
    static String read(File file) {

        String mass = "";
        try (FileReader reader = new FileReader(file)) {
            // читаем посимвольно
            int c;
            while ((c = reader.read()) != -1) {
                mass += (char) c;
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Проблема с файлом " + file.getName());
        }
        return mass;
    }
}